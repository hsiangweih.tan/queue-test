import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const port = 8001;
  const app = await NestFactory.create(AppModule);
  await app.listen(port);
  console.log(`App running on port: ${port}`)
}
bootstrap();
