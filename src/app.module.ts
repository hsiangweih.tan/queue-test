import { ConfigModule } from '@nestjs/config';
import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppConsumer } from './app.processor';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    BullModule.forRoot({
      redis: {
        host: process.env.QUEUE_HOST,
        port: +process.env.QUEUE_PORT,
        password: process.env.QUEUE_AUTH,
      },
    }),
    BullModule.registerQueue({
      name: 'testing-queue',
    }),
  ],
  controllers: [AppController],
  providers: [AppService, AppConsumer],
})
export class AppModule {}
