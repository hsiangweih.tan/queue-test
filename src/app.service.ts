import { Injectable } from '@nestjs/common';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@Injectable()
export class AppService {
  id = 0;
  constructor(@InjectQueue('testing-queue') private queue: Queue) {}
  async addQueue(): Promise<void> {
    console.log(`Adding to queue ${this.id}`);
    await this.queue.add({
      id: this.id++,
    });
  }
}
