import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';

export type Id = {
  id: string;
};
@Processor('testing-queue')
export class AppConsumer {
  @Process()
  async consumeQueue(job: Job<unknown>): Promise<void> {
    const { id } = job.data as Id;
    console.log(`Processing queue ${id}`)
    for (let i = 0; i <= 300; i++) {
      if(i%20 === 0 ){
        console.log(`Process running ${id}: ${i}`);
      }
      await this.timeout(200);
    }
    console.log(`Finish Processing`);
  }

  timeout(ms: number): Promise<unknown> {
    // eslint-disable-next-line @typescript-eslint/typedef
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
